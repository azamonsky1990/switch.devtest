﻿using System;
using NUnit.Framework;
using SharpBank.AccountData;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private Bank bank;
        private DateTime dateNow;

        [SetUp]
        public void Initialize()
        {
            this.bank = new Bank();
            this.dateNow = DateTime.Now;
        }

        [Test]
        public void Test_AddCustomer()
        {
            string name = "test";
            Customer customer = this.bank.AddCustomer(name);
            Assert.AreEqual(name, customer.Name);
        }

        [Test]
        public void Test_GetCustomerSummary()
        {
            string customer1Name = "customer1";
            string customer2Name = "customer2";
            Customer customer1 = this.bank.AddCustomer(customer1Name);
            Customer customer2 = this.bank.AddCustomer(customer2Name);

            customer1.OpenAccount(AccountData.AccountType.CHECKING_ACCOUNT, "CHECKING_ACCOUNT");
            customer2.OpenAccount(AccountData.AccountType.CHECKING_ACCOUNT, "CHECKING_ACCOUNT");
            customer2.OpenAccount(AccountData.AccountType.MAXI_SAVINGS_ACCOUNT, "MAXI_SAVINGS_ACCOUNT");

            string expected = "Customer Summary\r\n - " + customer1.ToString() + "\r\n - " + customer2.ToString();
            Assert.AreEqual(expected, this.bank.GetCustomerSummary());
        }

        [Test]
        public void Test_TotalInterestPaid()
        {
            string customer1Name = "customer1";
            string customer2Name = "customer2";

            string account1Id = "Account1";
            string account2Id = "Account2";
            string account3Id = "Account3";
            string account4Id = "Account4";

            Customer customer1 = this.bank.AddCustomer(customer1Name);
            Customer customer2 = this.bank.AddCustomer(customer2Name);

            Account account1 = customer1.OpenAccount(AccountType.CHECKING_ACCOUNT, account1Id);
            Account account2 = customer1.OpenAccount(AccountType.MAXI_SAVINGS_ACCOUNT, account2Id);
            Account account3 = customer2.OpenAccount(AccountType.SAVINGS_ACCOUNT, account3Id);
            Account account4 = customer2.OpenAccount(AccountType.SAVINGS_ACCOUNT, account4Id);

            customer1.Deposit(account1Id, 100, dateNow);
            customer1.Deposit(account2Id, 200, dateNow);
            customer2.Deposit(account3Id, 500, dateNow);
            customer2.Deposit(account4Id, 600, dateNow);
            customer1.Deposit(account2Id, 800, dateNow);
            customer2.Deposit(account3Id, 1000, dateNow);
            customer1.Withdraw(account2Id, 50, dateNow);
            customer2.Withdraw(account3Id, 60, dateNow);

            double expected = customer1.TotalInterestEarned(dateNow) + customer2.TotalInterestEarned(dateNow);
            Assert.AreEqual(expected, this.bank.TotalInterestPaid(dateNow), TestConstants.DOUBLE_DELTA);
        }
    }
}
