﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class UtilsTest
    {
       
        [Test]
        public void Test_ToDollars_True()
        {
            double value = 10; 
            Assert.AreEqual("$10,00", Utils.ToDollars(value));
        }

        [Test]
        public void Test_ToDollars_False()
        {
            double value = 10;
            Assert.AreNotEqual("$10", Utils.ToDollars(value));
        }

        [Test]
        public void Test_Format_Plural_True()
        {
            Assert.AreEqual("2 tests", Utils.Format(2, "test"));
        }

        [Test]
        public void Test_Format_Plural_False()
        {
            Assert.AreNotEqual("2 test", Utils.Format(2, "test"));
        }

        [Test]
        public void Test_Format_Singular_True()
        {
            Assert.AreEqual("1 test", Utils.Format(1, "test"));
        }

        [Test]
        public void Test_Format_Singular_False()
        {
            Assert.AreNotEqual("1 tests", Utils.Format(1, "test"));
        }

    }
}
