﻿using System;
using NUnit.Framework;
using SharpBank.AccountData;

namespace SharpBank.Test.AccountTests
{
    [TestFixture]
    class MaxiSavingsAccountTest : AccountTest
    {
        [SetUp]
        public override void Initialize()
        {
            this.accountId = "MaxiSavingsAccount";
            this.account = new MaxiSavingsAccount(this.accountId);
            this.dateNow = DateTime.Now;
        }

        [Test]
        public override void Test_ToString()
        {
            Assert.AreEqual("Maxi Savings Account", this.account.ToString());
        }

        [Test]
        public override void Test_InterestEarned()
        {
            this.account.Deposit(2500, dateNow.AddDays(-20));
            this.account.Withdraw(1000, dateNow.AddDays(-15));

            //1500 * 0.05
            Assert.AreEqual(1500 * 0.05, this.account.InterestEarned(dateNow));

            this.account.Withdraw(1000, dateNow.AddDays(-5));
            
            //500 * 0.001
            Assert.AreEqual(500 * 0.001, this.account.InterestEarned(dateNow));
        }

        [Test]
        public override void Test_GetStatement()
        {
            this.account.Deposit(1000, DateTime.Now);
            this.account.Withdraw(500, DateTime.Now);
            string expected = "Maxi Savings Account\r\n  deposit $1.000,00\r\n  withdrawal $500,00\r\nTotal $500,00";
            Assert.AreEqual(expected, this.account.GetStatement());
        }

        [Test]
        public override void Test_CalculateAccrueInterest()
        {
            this.account.Deposit(2500, dateNow.AddDays(-20));
            this.account.Withdraw(1000, dateNow.AddDays(-15));
            this.account.Deposit(100, dateNow.AddDays(-4));
            //Accure Interest: 2500 * (0.05/365) * 5 dias + 1500 * (0.001/365) * 9 dias + 1500 * (0.05/365) * 2 dias + 1600 * (0.05/365) * 4 dias
            double expected = 2500 * (0.05 / 365) * 5 + 1500 * (0.001 / 365) * 9 + 1500 * (0.05 / 365) * 2 + 1600 * (0.05 / 365) * 4;
            Assert.AreEqual(expected, this.account.CalculateAccrueInterest(dateNow), TestConstants.DOUBLE_DELTA);
        }
       
    }
}
