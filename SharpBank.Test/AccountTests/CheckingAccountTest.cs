﻿using System;
using NUnit.Framework;
using SharpBank.AccountData;

namespace SharpBank.Test.AccountTests
{
    [TestFixture]
    public class CheckingAccountTest : AccountTest
    {
        [SetUp]
        public override void Initialize()
        {
            this.accountId = "CheckingAccount";
            this.account = AccountFactory.CreateAccount(AccountType.CHECKING_ACCOUNT, this.accountId);//new CheckingAccount(this.accountId); 
            this.dateNow = DateTime.Now;
        }

        [Test]
        public override void Test_ToString()
        {
            Assert.AreEqual("Checking Account", this.account.ToString());
        }

        [Test]
        public override void Test_InterestEarned()
        {
            this.account.Deposit(1000, dateNow);

            //1000*0.001
            Assert.AreEqual(1, this.account.InterestEarned(dateNow));
        }

        [Test]
        public override void Test_GetStatement()
        {
            this.account.Deposit(1000, dateNow);
            this.account.Withdraw(500, dateNow);
            string expected = "Checking Account\r\n  deposit $1.000,00\r\n  withdrawal $500,00\r\nTotal $500,00";
            Assert.AreEqual(expected, this.account.GetStatement());
        }

        [Test]
        public override void Test_CalculateAccrueInterest()
        {
            this.account.Deposit(1000, dateNow.AddDays(-10));
            this.account.Withdraw(500, dateNow.AddDays(-5));
            this.account.Deposit(100, dateNow.AddDays(-1));
            //Accure Interest: (1000 * 5 dias + 500 * 4 dias + 600 x 1 dia) *(0.001/365)
            double expected = (1000 * 5 + 500 * 4 + 600) * (0.001 / 365);
            Assert.AreEqual(expected, this.account.CalculateAccrueInterest(dateNow), TestConstants.DOUBLE_DELTA);
        }
    }
}
