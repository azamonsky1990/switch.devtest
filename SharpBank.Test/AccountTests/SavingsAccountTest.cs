﻿using System;
using NUnit.Framework;
using SharpBank.AccountData;

namespace SharpBank.Test.AccountTests
{
    [TestFixture]
    public class SavingsAccountTest : AccountTest
    {
        [SetUp]
        public override void Initialize()
        {
            this.accountId = "SavingsAccount";
            this.account = new SavingsAccount(this.accountId);
            this.dateNow = DateTime.Now;
        }

        [Test]
        public override void Test_ToString()
        {
            Assert.AreEqual("Savings Account", this.account.ToString());
        }

        [Test]
        public override void Test_InterestEarned()
        {
            this.account.Deposit(2500, dateNow);

            //1000*0.001 + 1500*0.002
            Assert.AreEqual(4, this.account.InterestEarned(dateNow));
        }

        [Test]
        public override void Test_GetStatement()
        {
            this.account.Deposit(1000, dateNow);
            this.account.Withdraw(500, dateNow);
            string expected = "Savings Account\r\n  deposit $1.000,00\r\n  withdrawal $500,00\r\nTotal $500,00";
            Assert.AreEqual(expected, this.account.GetStatement());
        }

        [Test]
        public override void Test_CalculateAccrueInterest()
        {
            this.account.Deposit(2000, dateNow.AddDays(-10));
            this.account.Withdraw(500, dateNow.AddDays(-5));
            this.account.Deposit(100, dateNow.AddDays(-1));
            //Accure Interest: 1000 * (0.001/365) * 10 dias + 1000 * (0.002/365) * 5 dias + 500 * (0.002/365) * 4 dias + 600 * (0.002/365) * 1 dia

            double expected = 1000 * (0.001 / 365) * 10 + (1000 * 5 + 500 * 4 + 600) * (0.002 / 365);
            Assert.AreEqual(expected, this.account.CalculateAccrueInterest(dateNow));
        }
    }
}
