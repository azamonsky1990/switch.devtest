﻿using System;
using System.Linq;
using NUnit.Framework;
using SharpBank.AccountData;
using SharpBank.Exceptions;

namespace SharpBank.Test.AccountTests
{
    [TestFixture]
    public abstract class AccountTest
    {
        protected string accountId;
        protected Account account;
        protected DateTime dateNow;

        [SetUp]
        public abstract void Initialize();

        [Test]
        public void Test_Id()
        {
            Assert.AreEqual(this.accountId, this.account.Id);
        }

        [Test]
        public void Test_Deposit_Success()
        {
            int deposit = 100;
            this.account.Deposit(deposit, dateNow);
            Assert.AreEqual(this.account.Transactions.Last().Amount, deposit);
        }

        [Test]
        public void Test_Deposit_Exception()
        {
            Assert.Throws<BusinessException>(() => account.Deposit(-100, dateNow));
        }

        [Test]
        public void Test_Withdraw_Success()
        {
            int withdraw = 100;
            this.account.Withdraw(withdraw, dateNow);
            Assert.AreEqual(this.account.Transactions.Last().Amount, -withdraw);
        }

        [Test]
        public void Test_Withdraw_Exception()
        {
            Assert.Throws<BusinessException>(() => account.Withdraw(-100, dateNow));
        }

        [Test]
        public void Test_SumTransactions()
        {
            this.account.Deposit(100, dateNow);
            this.account.Withdraw(50, dateNow);
            this.account.Deposit(300, dateNow);
            this.account.Withdraw(200, dateNow);
            //100-50+300-200
            Assert.AreEqual(150, this.account.SumTransactions());
        }

        [Test]
        public abstract void Test_ToString();

        [Test]
        public abstract void Test_InterestEarned();

        [Test]
        public abstract void Test_GetStatement();

        [Test]
        public abstract void Test_CalculateAccrueInterest();
    }
}
