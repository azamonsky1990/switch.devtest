﻿using System;
using System.Linq;
using NUnit.Framework;
using SharpBank.AccountData;
using SharpBank.Exceptions;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        private Customer customer;
        private string customerName;
        private DateTime dateNow;

        [SetUp]
        public void Initialize()
        {
            this.customerName = "test";
            this.customer = new Customer(this.customerName);
            this.dateNow = DateTime.Now;
        }

        [Test]
        public void Test_Name()
        {
            Assert.AreEqual(this.customerName, this.customer.Name);
        }

        [Test]
        public void Test_NumberOfAccounts()
        {
            string defaultId = "1";
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId);
            Assert.AreEqual(1, this.customer.NumberOfAccounts);
        }

        [Test]
        public void Test_OpenCheckingAccount()
        {
            Account account = this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, "CheckingAccount");
            Assert.AreEqual("CheckingAccount", account.Id);
        }

        [Test]
        public void Test_OpenSavingsAccount()
        {
            Account account = this.customer.OpenAccount(AccountType.SAVINGS_ACCOUNT, "SavingsAccount");
            Assert.AreEqual("SavingsAccount", account.Id);
        }

        [Test]
        public void Test_OpenMaxiSavingsAccount()
        {
            Account account = this.customer.OpenAccount(AccountType.MAXI_SAVINGS_ACCOUNT, "MaxiSavingsAccount");
            Assert.AreEqual("MaxiSavingsAccount", account.Id);
        }

        [Test]
        public void Test_OpenAccount_Exception()
        {
            string defaultId = "1";
            Account account = this.customer.OpenAccount(AccountType.MAXI_SAVINGS_ACCOUNT, defaultId);
            Assert.Throws<BusinessException>(() => this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId));
        }

        [Test]
        public void Test_GetAccount()
        {
            string defaultId = "1";
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId);
            Account account = this.customer.GetAccount(defaultId);
            Assert.AreEqual(defaultId, account.Id);
        }

        [Test]
        public void Test_GetAccount_Null()
        {
            string defaultId = "1";
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId);
            Account account = this.customer.GetAccount("2");
            Assert.IsNull(account);
        }

        [Test]
        public void Test_Deposit_Success()
        {
            string defaultId = "1";
            int deposit = 100;
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId);
            this.customer.Deposit(defaultId, deposit, dateNow);
            Account account = this.customer.GetAccount(defaultId);
            Assert.AreEqual(account.Transactions.Last().Amount, deposit);
        }

        [Test]
        public void Test_Deposit_Exception()
        {
            string defaultId = "1";
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId);
            Assert.Throws<BusinessException>(() => this.customer.Deposit(defaultId, -100, dateNow));
        }

        [Test]
        public void Test_Withdraw_Success()
        {
            string defaultId = "1";
            int withdraw = 100;
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId);
            this.customer.Withdraw(defaultId, withdraw, dateNow);
            Account account = this.customer.GetAccount(defaultId);
            Assert.AreEqual(account.Transactions.Last().Amount, -withdraw);
        }

        [Test]
        public void Test_Withdraw_Exception()
        {
            string defaultId = "1";
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, defaultId);
            Assert.Throws<BusinessException>(() => this.customer.Withdraw(defaultId, -100, dateNow));
        }

        [Test]
        public void Test_Transfer()
        {
            string sourceId = "CHECKING_ACCOUNT";
            string targetId = "MAXI_SAVINGS_ACCOUNT";
            int transferAmount = 100;
            int defaultAmount = 500;

            Account source = this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, sourceId);
            Account target = this.customer.OpenAccount(AccountType.MAXI_SAVINGS_ACCOUNT, targetId);

            this.customer.Deposit(source.Id, defaultAmount, dateNow);
            this.customer.Deposit(target.Id, defaultAmount, dateNow);
            this.customer.Transfer(source.Id, target.Id, transferAmount, dateNow);

            Assert.AreEqual(defaultAmount - transferAmount, source.SumTransactions());
            Assert.AreEqual(defaultAmount + transferAmount, target.SumTransactions());
        }

        [Test]
        public void Test_GetStatement()
        {
            string account1Id = "CHECKING_ACCOUNT";
            string account2Id = "MAXI_SAVINGS_ACCOUNT";
            int amount1 = 100;
            int amount2 = 500;

            Account account1 = this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, account1Id);
            Account account2 = this.customer.OpenAccount(AccountType.MAXI_SAVINGS_ACCOUNT, account2Id);

            this.customer.Deposit(account1Id, amount1, dateNow);
            this.customer.Deposit(account2Id, amount2, dateNow);

            string expected = "Statement for " + this.customerName + "\r\n\r\n";
            expected += account1.GetStatement() + "\r\n\r\n" + account2.GetStatement() + "\r\n\r\n";
            expected += "Total In All Accounts $600,00";

            Assert.AreEqual(expected, this.customer.GetStatement());
        }

        [Test]
        public void Test_TotalInterestEarned()
        {
            string account1Id = "CHECKING_ACCOUNT";
            string account2Id = "MAXI_SAVINGS_ACCOUNT";
            string account3Id = "SAVINGS_ACCOUNT";
            int amount1 = 100;
            int amount2 = 500;
            int amount3 = 2000;

            Account account1 = this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, account1Id);
            Account account2 = this.customer.OpenAccount(AccountType.MAXI_SAVINGS_ACCOUNT, account2Id);
            Account account3 = this.customer.OpenAccount(AccountType.SAVINGS_ACCOUNT, account3Id);

            this.customer.Deposit(account1Id, amount1, dateNow);
            this.customer.Deposit(account2Id, amount2, dateNow);
            this.customer.Deposit(account3Id, amount3, dateNow);

            double expected = account1.InterestEarned(dateNow) + account2.InterestEarned(dateNow) + account3.InterestEarned(dateNow);
            Assert.AreEqual(expected, this.customer.TotalInterestEarned(dateNow), TestConstants.DOUBLE_DELTA);
        }

        [Test]
        public void Test_ToString()
        {
            this.customer.OpenAccount(AccountType.CHECKING_ACCOUNT, "CHECKING_ACCOUNT");
            this.customer.OpenAccount(AccountType.SAVINGS_ACCOUNT, "SAVINGS_ACCOUNT");
            Assert.AreEqual(this.customer.Name + " (2 accounts)", this.customer.ToString());
        }

    }
}
