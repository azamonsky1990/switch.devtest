﻿using System;
using NUnit.Framework;
using SharpBank.TransactionData;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        private double amount;
        private DateTime dateNow;
        private Transaction depositTransaction;
        private Transaction withdrawTransaction;

        [SetUp]
        public void Initialize()
        {
            this.amount = 100;
            this.dateNow = DateTime.Now;
            this.depositTransaction = new Transaction(this.amount, this.dateNow);
            this.withdrawTransaction = new Transaction(- this.amount, this.dateNow);
        }

        [Test]
        public void Test_Amount()
        {
            Assert.AreEqual(100, this.depositTransaction.Amount);
            Assert.AreEqual(-100, this.withdrawTransaction.Amount);
        }

        [Test]
        public void Test_Deposit_TransactionDate()
        {
            Assert.AreEqual(this.dateNow, this.depositTransaction.TransactionDate);
            Assert.AreEqual(this.dateNow, this.withdrawTransaction.TransactionDate);
        }

        [Test]
        public void Test_Deposit_Type()
        {
            Assert.AreEqual(TransactionType.DEPOSIT, this.depositTransaction.TransactionType);
        }

        [Test]
        public void Test_Withdraw_Type()
        {
            Assert.AreEqual(TransactionType.WITHDRAW, this.withdrawTransaction.TransactionType);
        }

        [Test]
        public void Test_Deposit_ToString()
        {
            Assert.AreEqual("deposit $100,00", this.depositTransaction.ToString());
        }

        [Test]
        public void Test_Withdraw_ToString()
        {
            Assert.AreEqual("withdrawal $100,00", this.withdrawTransaction.ToString());
        }

    }
}
