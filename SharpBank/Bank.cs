﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        #region Private Attributes

        private List<Customer> customers;

        #endregion

        #region Constructors

        public Bank()
        {
            this.customers = new List<Customer>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates a cusomer with the provided name
        /// </summary>
        /// <param name="customer"></param>
        public Customer AddCustomer(string name)
        {
            Customer customer = new Customer(name);
            this.customers.Add(customer);
            return customer;
        }

        /// <summary>
        /// Returns the Customer Summary
        /// </summary>
        /// <returns></returns>
        public string GetCustomerSummary()
        {
            StringBuilder stringBuilder = new StringBuilder("Customer Summary");
            this.customers.ForEach(customer =>
            {
                stringBuilder.AppendLine().Append(" - " + customer.ToString());
            });

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Total interest paied to all customers of the bank
        /// </summary>
        /// <returns></returns>
        public double TotalInterestPaid(DateTime date)
        {
            return this.customers.Sum(customer => customer.TotalInterestEarned(date));
        }

        #endregion

    }
}
