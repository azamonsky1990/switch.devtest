﻿using System;

namespace SharpBank
{
    public static class Utils
    {
        /// <summary>
        /// Format double as dollar string
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static string ToDollars(double d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        /// <summary>
        ///Make sure correct plural of word is created based on the number passed in:
        ///If number passed in is 1 just return the word otherwise add an 's' at the end
        /// </summary>
        /// <param name="number"></param>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string Format(int number, string word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }
    }
}
