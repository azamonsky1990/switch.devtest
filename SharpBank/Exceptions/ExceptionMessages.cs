﻿namespace SharpBank.Exceptions
{
    internal static class ExceptionMessages
    {
        public const string NEGATIVE_AMOUNT = "amount must be greater than zero";
        public const string ORIGIN_ACCOUNT_DOES_NOT_EXIST = "Origin account does not exists";
        public const string TARGET_ACCOUNT_DOES_NOT_EXIST = "Target account does not exists";
        public const string NO_ACCOUNT_FOR_PROVIDED_TYPE = "No account for the provided type";
        public const string ACCOUNT_ALREADY_EXISTS = "Account already exists";
    }
}
