﻿using System;

namespace SharpBank.TransactionData
{
    public class Transaction
    {
        #region Attributes

        private readonly double amount;
        private readonly DateTime transactionDate;
        private readonly TransactionType transactionType;

        #endregion

        #region Constructors

        public Transaction(double amount, DateTime date)
        {
            this.amount = amount;
            this.transactionDate = date;
            this.transactionType = this.amount < 0 ? TransactionType.WITHDRAW : TransactionType.DEPOSIT;
        }

        #endregion

        #region Public Properties

        public double Amount
        {
            get
            {
                return this.amount;
            }
        }

        public TransactionType TransactionType
        {
            get
            {
                return this.transactionType;
            }
        }

        public DateTime TransactionDate
        {
            get
            {
                return this.transactionDate;
            }
        }

        #endregion

        #region Private Properties

        private string Type
        {
            get
            {
                switch (transactionType)
                {
                    case TransactionType.WITHDRAW:
                        return "withdrawal";
                    case TransactionType.DEPOSIT:
                        return "deposit";
                    default:
                        return "";
                }
            }
        }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return this.Type + " " + Utils.ToDollars(this.amount);
        }

        #endregion
    }
}
