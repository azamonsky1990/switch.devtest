﻿namespace SharpBank.TransactionData
{
    public enum TransactionType
    {
        DEPOSIT = 1,
        WITHDRAW = 2
    }
}