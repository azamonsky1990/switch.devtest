﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpBank.AccountData;
using SharpBank.Exceptions;

namespace SharpBank
{
    public class Customer
    {
        #region Attributes

        private string name;
        private List<Account> accounts;

        #endregion

        #region Constructor

        public Customer(string name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        #endregion

        #region Public Properties

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public int NumberOfAccounts
        {
            get
            {
                return this.accounts.Count;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates a new Account
        /// </summary>
        /// <param name="account"></param>
        public Account OpenAccount(AccountType accountType, string id)
        {
            if (this.accounts.Exists(a => a.Id == id))
            {
                throw new BusinessException(ExceptionMessages.ACCOUNT_ALREADY_EXISTS);
            }

            Account account = AccountFactory.CreateAccount(accountType, id);
            this.accounts.Add(account);
            return account;
        }

        /// <summary>
        /// Gets the specified account.. If it is not found returns null
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Account GetAccount(string accountId)
        {
            return this.accounts.FirstOrDefault(a => a.Id.Equals(accountId));
        }

        /// <summary>
        /// Deposit the amout in the specified account
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="amount"></param>
        public void Deposit(string accountId, double amount, DateTime date)
        {
            Account account = this.GetAccount(accountId);
            if (account == null)
            {
                throw new BusinessException(ExceptionMessages.TARGET_ACCOUNT_DOES_NOT_EXIST);
            }

            account.Deposit(amount, date);
        }

        /// <summary>
        /// Withdraw the amout in the specified account
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="amount"></param>
        public void Withdraw(string accountId, double amount, DateTime date)
        {
            Account account = this.GetAccount(accountId);
            if (account == null)
            {
                throw new BusinessException(ExceptionMessages.ORIGIN_ACCOUNT_DOES_NOT_EXIST);
            }

            account.Withdraw(amount, date);
        }

        /// <summary>
        /// Transfer amount from origin to target
        /// </summary>
        /// <param name="originAccountId"></param>
        /// <param name="targetAccountId"></param>
        /// <param name="amount"></param>
        public void Transfer(string originAccountId, string targetAccountId, double amount, DateTime date)
        {
            this.Withdraw(originAccountId, amount, date);
            this.Deposit(targetAccountId, amount, date);
        }

        /// <summary>
        /// Gets the customer statement
        /// </summary>
        /// <returns></returns>
        public string GetStatement()
        {
            StringBuilder stringBuilder = new StringBuilder("Statement for " + this.name);
            stringBuilder.AppendLine();

            double total = 0;
            this.accounts.ForEach(account =>
            {
                stringBuilder.AppendLine().Append(account.GetStatement()).AppendLine();
                total += account.SumTransactions();
            });

            stringBuilder.AppendLine().Append("Total In All Accounts " + Utils.ToDollars(total));
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Total interest earned in the Customer
        /// </summary>
        /// <returns></returns>
        public double TotalInterestEarned(DateTime date)
        {
            return this.accounts.Sum(account => account.InterestEarned(date));
        }

        /// <summary>
        /// To String Method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name + " (" + Utils.Format(this.NumberOfAccounts, "account") + ")";
        }

        #endregion
    }
}
