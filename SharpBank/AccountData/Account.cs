﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpBank.Exceptions;
using SharpBank.TransactionData;

namespace SharpBank.AccountData
{
    public abstract class Account
    {
        #region Constants
        protected const int INTEREST_PERIOD_IN_DAYS = 365;

        #endregion

        #region Attributes
        private readonly string id;
        protected List<Transaction> transactions;
        #endregion

        #region Constructors
        public Account(string id)
        {
            this.id = id;
            this.transactions = new List<Transaction>();
        }
        #endregion

        #region Properties
        public string Id
        {
            get
            {
                return this.id;
            }
        }

        public List<Transaction> Transactions
        {
            get
            {
                return this.transactions;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Deposits the indicated amount
        /// </summary>
        /// <returns>Amount to deposit</returns>
        public void Deposit(double amount, DateTime date)
        {
            if (amount <= 0)
            {
                throw new BusinessException(ExceptionMessages.NEGATIVE_AMOUNT);
            }

            transactions.Add(new Transaction(amount, date));
        }

        /// <summary>
        /// Withdraw the indicated amount
        /// </summary>
        /// <returns>Amount to withdraw</returns>
        public void Withdraw(double amount, DateTime date)
        {
            if (amount <= 0)
            {
                throw new BusinessException(ExceptionMessages.NEGATIVE_AMOUNT);
            }

            transactions.Add(new Transaction(-amount, date));
        }

        /// <summary>
        /// Sums al transactions of current Account
        /// </summary>
        /// <returns>Sum of all transactions</returns>
        public double SumTransactions()
        {
            return this.SumTransactions(this.transactions);
        }

        /// <summary>
        /// Calculates the interest earned based in all transactions
        /// </summary>
        /// <returns>Interest earned with all transactions</returns>
        public double InterestEarned(DateTime date)
        {
            double amount = this.SumTransactions();
            return this.CalculateInterest(amount, date);
        }

        /// <summary>
        /// Gnerates a statement based in the transactions
        /// </summary>
        /// <returns>Statement of Account</returns>
        public string GetStatement()
        {
            StringBuilder stringBuilder = new StringBuilder(this.ToString());
            stringBuilder.AppendLine();
            stringBuilder.Append(this.GenerateResume());
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Calculates the Accrue interest until current date
        /// </summary>
        /// <returns></returns>
        public double CalculateAccrueInterest(DateTime current)
        {
            double interest = 0;
            Transaction firstDeposit = this.transactions.FindAll(t => t.TransactionType == TransactionType.DEPOSIT).OrderBy(t => t.TransactionDate).FirstOrDefault();

            if (firstDeposit != null)
            {
                DateTime endDateTime = firstDeposit.TransactionDate.AddDays(1);
                while (endDateTime <= current)
                {
                    List<Transaction> currentTransactions = this.transactions.FindAll(t => t.TransactionDate >= firstDeposit.TransactionDate && t.TransactionDate < endDateTime);
                    double amount = this.SumTransactions(currentTransactions);
                    interest += this.CalculateDayInterest(amount, endDateTime);
                    endDateTime = endDateTime.AddDays(1);
                }
            }

            return interest;
        }

        #endregion

        #region Protected methods
        /// <summary>
        /// Returns all transactions that are previous to @maxDate at least by one day
        /// </summary>
        /// <param name="maxDate">Max Sate</param>
        /// <returns></returns>
        protected List<Transaction> GetPrevTransactionsByOneDayOfDifference(DateTime maxDate)
        {
            return this.transactions.FindAll(transaction => (maxDate - transaction.TransactionDate).TotalDays > 1);
        }

        /// <summary>
        /// Calculate the interest of the given amount
        /// </summary>
        /// <param name="amount">Amount to calculate interest</param>
        /// <param name="date">Date reference</param>
        /// <returns>Interest of given amount</returns>
        protected abstract double CalculateInterest(double amount, DateTime date);

        /// <summary>
        /// Calculates the day interest generated by a transaction
        /// </summary>
        /// <param name="amount">
        /// Amount to calculate interest</param>
        /// <param name="maxDate">
        /// Max date until interest calculation</param>
        /// <returns>Calculated interest</returns>
        protected abstract double CalculateDayInterest(double amount, DateTime maxDate);

        #endregion

        #region Private methods
        /// <summary>
        /// Generates the a stringBuilder with the resume
        /// </summary>
        /// <returns></returns>
        private StringBuilder GenerateResume()
        {
            StringBuilder stringBuilder = new StringBuilder();

            double total = 0;
            this.transactions.ForEach(transaction =>
            {
                stringBuilder.Append("  " + transaction.ToString()).AppendLine();
                total += transaction.Amount;
            });

            stringBuilder.Append("Total " + Utils.ToDollars(total));
            return stringBuilder;
        }

        /// <summary>
        /// Sum of all given transactions
        /// </summary>
        /// <param name="currentTransactions"></param>
        /// <returns></returns>
        public double SumTransactions(List<Transaction> currentTransactions)
        {
            return currentTransactions.Sum(transaction => transaction.Amount);
        }

        #endregion
    }
}
