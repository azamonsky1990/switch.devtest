﻿using System;

namespace SharpBank.AccountData
{
    public class SavingsAccount : Account
    {
        #region Constants

        private const double PRIMARY_RATE = 0.001;
        private const double SECONDARY_RATE = 0.002;
        private const double AMOUNT_STEP = 1000;

        #endregion

        #region Constructors

        public SavingsAccount(string id) : base(id)
        { }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return "Savings Account";
        }

        #endregion

        #region Protected Methods

        protected override double CalculateInterest(double amount, DateTime date)
        {
            return this.CalculateInterest(amount, date, PRIMARY_RATE, SECONDARY_RATE);
        }

        protected override double CalculateDayInterest(double amount, DateTime date)
        {
            return this.CalculateInterest(amount, date, PRIMARY_RATE / INTEREST_PERIOD_IN_DAYS, SECONDARY_RATE / INTEREST_PERIOD_IN_DAYS);
        }

        #endregion

        #region Private Methods

        private double CalculateInterest(double amount, DateTime date, double primaryRate, double secodaryRate)
        {
            if (amount <= AMOUNT_STEP)
            {
                return amount * primaryRate;
            }

            double partialInterest = AMOUNT_STEP * primaryRate;
            double restOfAmount = amount - AMOUNT_STEP;

            return partialInterest + restOfAmount * secodaryRate;
        }

        #endregion
    }
}
