﻿using System;
using System.Linq;
using SharpBank.TransactionData;

namespace SharpBank.AccountData
{
    public class MaxiSavingsAccount : Account
    {
        #region Constants
        private const double PRIMARY_RATE = 0.05;
        private const double SECONDARY_RATE = 0.001;
        private const double TIME_STEP = 10;
        #endregion

        #region Constructors

        public MaxiSavingsAccount(string id) : base(id)
        { }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return "Maxi Savings Account";
        }

        #endregion

        #region Protected Methods

        protected override double CalculateInterest(double amount, DateTime date)
        {
            return this.CalculateInterest(amount, date, PRIMARY_RATE, SECONDARY_RATE);
        }

        protected override double CalculateDayInterest(double amount, DateTime date)
        {
            return this.CalculateInterest(amount, date, PRIMARY_RATE / INTEREST_PERIOD_IN_DAYS, SECONDARY_RATE / INTEREST_PERIOD_IN_DAYS);
        }

        #endregion

        #region Private Methods

        private bool NoWithdrawsInTimeStep(DateTime date)
        {
            Transaction lastWithdraw = this.transactions.FindAll(t => t.TransactionType == TransactionType.WITHDRAW && t.TransactionDate < date)
                .OrderByDescending(t => t.TransactionDate).FirstOrDefault();
            if (lastWithdraw == null)
            {
                return true;
            }

            return (date - lastWithdraw.TransactionDate).Days >= TIME_STEP;

        }

        private double CalculateInterest(double amount, DateTime date, double primaryRate, double secodaryRate)
        {
            if (this.NoWithdrawsInTimeStep(date))
            {
                return amount * primaryRate;
            }

            return amount * secodaryRate;
        }

        #endregion
    }
}
