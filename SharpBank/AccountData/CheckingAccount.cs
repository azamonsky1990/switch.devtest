﻿using System;

namespace SharpBank.AccountData
{
    public class CheckingAccount : Account
    {
        #region Constants

        private const double RATE = 0.001;

        #endregion

        #region Constructors

        public CheckingAccount(string id) : base(id)
        { }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return "Checking Account";
        }

        #endregion

        #region Protected Methods

        protected override double CalculateInterest(double amount, DateTime date)
        {
            return this.CalculateInterest(amount, RATE);
        }

        protected override double CalculateDayInterest(double amount, DateTime date)
        {
            return this.CalculateInterest(amount, RATE / INTEREST_PERIOD_IN_DAYS);
        }

        #endregion

        #region Private Methods

        private double CalculateInterest(double amount, double rate)
        {
            return amount * rate;
        }

        #endregion
    }
}
