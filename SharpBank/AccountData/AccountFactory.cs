﻿using SharpBank.Exceptions;

namespace SharpBank.AccountData
{
    public static class AccountFactory
    {
        public static Account CreateAccount(AccountType accountType, string id)
        {
            switch (accountType)
            {
                case AccountType.CHECKING_ACCOUNT:
                    return new CheckingAccount(id);

                case AccountType.SAVINGS_ACCOUNT:
                    return new SavingsAccount(id);

                case AccountType.MAXI_SAVINGS_ACCOUNT:
                    return new MaxiSavingsAccount(id);

                default:
                    throw new BusinessException(ExceptionMessages.NO_ACCOUNT_FOR_PROVIDED_TYPE);
            }
        }
    }
}
