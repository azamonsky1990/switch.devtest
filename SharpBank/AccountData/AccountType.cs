﻿namespace SharpBank.AccountData
{
    public enum AccountType
    {
        CHECKING_ACCOUNT = 0,
        SAVINGS_ACCOUNT = 1,
        MAXI_SAVINGS_ACCOUNT = 2
    }
}